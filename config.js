const res=require('dotenv').config();
if (res.error) {
    console.error(res.error);
}
else {
    module.exports = {
        client: process.env.CLIENT,
        database: process.env.DATABASE,
        password: process.env.PASSWORD,
        user: process.env.USER,
        SECRET_KEY:process.env.SECRET_KEY,
        DBCONFIG:process.env.DBCONFIG
    };
}