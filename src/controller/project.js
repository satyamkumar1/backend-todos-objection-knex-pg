const express = require("express");
const Router = express.Router();
const Project = require("../db/models/project");
const { authenticateToken } = require("../authentication/tokenAuth");

//get all Project data

Router.get("/", authenticateToken, (req, res) => {
  console.log(req.user);
  try {
    Project.query()
      .where("user_id", req.user.id)
      .withGraphFetched("tasks")
      .then((users) => {
        if (!users) {
          res.status(404).json({ msg: "project not found" });
        } else {
          res.status(200).json({ success: true, data: users });
        }
      });
  } catch (err) {
    res.status(500).json({ success: false, msg: err.message });
  }
});

// get project by id

Router.get("/:id", authenticateToken, (req, res) => {
  const { id } = req.params;
  try {
    Project.query()
      .findById(id)
      .withGraphFetched("tasks")
      .then((users) => {
        // console.log(users);
        if (!users) {
          res.status(404).json({ msg: " project not found" });
        } else {
          // console.log(req.user,users.user_id);
          if (req.user.id === users.user_id) {
            res.json({ success: true, data: users });
          } else {
            res.status(400).json({ msg: "authentication failed" });
          }
        }
      });
  } catch (err) {
    res.status(500).json({ success: false, msg: err.message });
  }
});

//post Project data

Router.post("/", authenticateToken, (req, res) => {
  const title = req.body.title;
  const description = req.body.description;
  const id = req.body.user_id;
  console.log(req.body);

  if (req.user == undefined) {
    return res.json({ msg: "authentication failed" });
  }
  Project.query()
    .insert({
      title: title,
      description: description,
      project_status: false,
      user_id: req.user.id,
    })
    .then(() => {
      res.json({ success: true, msg: " project created successfully" });
    })
    .catch((err) => {
      res.status(500).json({ success: false, msg: err.message });
    });
});

//update Project data

Router.put("/:id", authenticateToken, (req, res) => {
  const { id } = req.params;
  const title = req.body.title;
  const description = req.body.description;

  Project.query()
    .findById(id)
    .patch({
      title: title,
      description: description,
    })
    .then((users) => {
      if (req.user.id === users) {
        res.json({ success: true, msg: "project updated successfully" });
      } else {
        res.status(400).json({ msg: "authentication failed" });
      }
    })
    .catch((err) => {
      res.status(500).json({ success: false, msg: err.message });
    });
});

//delete Project data

Router.delete("/:id", authenticateToken, (req, res) => {
  const { id } = req.params;

  Project.query()
    .deleteById(id)
    .then(() => {
      console.log(req.user.id, req.body.user_id);
      if (req.user.id == req.body.user_id) {
        res.json({ success: true, data: "project deleted successfully" });
      } else {
        res.status(400).json({ msg: "authentication failed" });
      }
    })
    .catch((err) => {
      res.status(500).json({ success: false, msg: err.message });
    });
});

module.exports = Router;
