const express = require("express");
const Router = express.Router();
const User = require("../db/models/users");
const {  hashPassword,  comparePassword,} = require("../authentication/bcrypthash");
const { generateToken } = require("../authentication/tokenAuth");

//get User data

Router.get("/:name", async (req, res) => {
  const { name } = req.params;
  try {
    const users = await User.query().where("name", name);
    if (users.length == 0) {
      res.status(404).json({ msg: " user not found" });
    } else {
      const token = generateToken(users);
      // console.log(users);
      const datajson = {
        user: users[0].name,
        email: users[0].email,
      };
      res.json({ success: true, data: datajson });
    }
  } catch (err) {
    res.status(500).json({ success: false, msg: err.message });
  }
});

//post User data

Router.post("/", async (req, res) => {
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;

  try {
    const hashpswd = await hashPassword(password);

    await User.query().insert({
      name: name,
      email: email,
      password: hashpswd,
    });
    res.json({ success: true, msg: "user created successfully" });
  } catch (err) {
    res.status(500).json({ success: false, data: err });
  }
});

//upadate user data

Router.put("/:id", (req, res) => {
  try {
    User.query()
      .where("id", req.params)
      .patch({
        name: req.body.name,
        email: req.body.email,
      })
      .then(() => {
        res.json({ success: true, msg: "user updated successfully" });
      });
  } catch (err) {
    res.status(500).json({ success: false, msg: err.message });
  }
});

//delete User data

Router.delete("/:id", (req, res) => {
  try {
    User.query()
      .deleteById(req.body.id)
      .then(() => {
        res.json({ success: true, msg: "user deleted successfully" });
      });
  } catch (err) {
    res.status(500).json({ success: false, msg: err.message });
  }
});

// user login using authentication and token

Router.post("/login", async (req, res) => {
  const { name, password } = req.body;

  try {
    const user = await User.query().findOne({ name });

    const passwordmatch = await comparePassword(password, user.password);
    // console.log(user);
    if (!passwordmatch) {
      return res
        .status(400)
        .json({ success: false, msg: "Invalid credentials" });
    }

    const token = generateToken([user]);
    res.json({ success: true, msg:"user login successfully", token: token });
  } catch (err) {
    res.status(500).json({ success: false, msg: err.message });
  }
});

module.exports = Router;
