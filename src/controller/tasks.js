const express = require("express");
const Router = express.Router();
const Task = require("../db/models/tasks");
const Project = require("../db/models/project");
const { authenticateToken } = require("../authentication/tokenAuth");

//get all Task

Router.get("/", authenticateToken,(req, res) => {
  try {
    Task.query().where('user_id',req.user.id).then((users) => {
      if (!users) {
        res.status(404).json({ msg: " tasks not found" });
      } else {
        res.status(200).json({ success: true, data: users });
      }
    });
  } catch (err) {
    res.status(500).json({ success: false, msg: err });
  }
});

//get Task by id data

Router.get("/:id", authenticateToken, (req, res) => {
  const { id } = req.params;
  try {
    Task.query()
      .findById(id)
      .then((users) => {
        if (!users) {
          res.status(404).json({ msg: "tasks not found" });
        } else {
          if (req.user.id === users.user_id) {
            res.json({ success: true, data: users });
          } else {
            res.status(404).json({ msg: "authentication failed" });
          }
        }
      });
  } catch (err) {
    res.status(500).json({ success: false, msg: err });
  }
});

//post Task data

Router.post("/", authenticateToken, (req, res) => {
  const title = req.body.title;
  const description = req.body.description;
  const pid = req.body.project_id;

  Task.query()
    .insert({
      title: title,
      description: description,
      task_status: false,
      user_id: req.user.id,
      project_id: pid,
    })
    .then(() => {
      res
        .status(200)
        .json({ success: true, msg: "tasks created successfully" });
    })
    .catch((err) => {
      res.status(500).json({ success: false, msg: err.message });
    });
});

//update Task data

Router.put("/:id", authenticateToken, (req, res) => {
  const { id } = req.params;
  const title = req.body.title;
  const description = req.body.description;

  try {
    Task.query()
      .findById(id)
      .then((data) => {
        if (data == undefined) {
          return res.status(400).json({ msg: "tasks doesn't exits" });
        }
        // console.log(data, id, "====", req.user);
        if (data.user_id === req.user.id) {
          Task.query()
            .findById(id)
            .patch({
              title: title,
              description: description,
            })
            .then(() => {
              // getAndLogTasks(id);
              res.json({ success: true, msg: "tasks updated successfully" });
            });
        } else {
          res.status(404).json({ msg: "authentication failed" });
        }
      });
  } catch (err) {
    res.status(500).json({ success: false, msg: err.message });
  }
});

//delete Task data

Router.delete("/:id", (req, res) => {
  const { id } = req.params;
  try {
    Task.query()
      .deleteById(id)
      .then(() => {
        // getAndLogTasks(id);
        res.json({ success: true, msg: "tasks deleted successfully" });
      });
  } catch (err) {
    res.status(500).json({ success: false, msg: err.message });
  }
});

//check all task status is true or not

async function getAndLogTasks(id) {
  try {
    let arr = await Task.query().findById(id);
    console.log(arr);
    const filterarr = arr.filter((data) => {
      return data.task_status == true;
    });

    if (arr.length == filterarr.length) {
      console.log(true);
      Project.query().findById(id).patch({
        project_status: true,
      });
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = Router;
