const knex = require("knex");
const knexfile = require("../../knexfile");
const { Model } = require("objection");

function dbsetup() {
  const db = knex(knexfile.development);
  Model.knex(db);

  db.raw("SELECT 1")
    .then(() => {
      console.log("Database connected successfully");
    })
    .catch((error) => {
      console.error("Error connecting to database:", error);
    });
}

module.exports = dbsetup;
