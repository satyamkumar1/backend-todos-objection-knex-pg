exports.up = function (knex) {
  return knex.schema
    .createTable("users", function (table) {
      table.increments("id").primary();
      table.string("name", 255).notNullable();
      table.string("email", 255).notNullable();
      table.string("password", 255).notNullable();
    })
    .createTable("project", function (table) {
      table.increments("id").primary();
      table.string("title", 255).notNullable();
      table.string("description", 255).notNullable();
      table.boolean("project_status").notNullable();
      table.integer("user_id").references("id").inTable("users");
    })
    .createTable("tasks", function (table) {
      table.increments("id").primary();
      table.string("title", 255).notNullable();
      table.string("description", 255).notNullable();
      table.boolean("task_status").notNullable();
      table.integer("user_id").references("id").inTable("users");
      table
        .integer("project_id")
        .references("id")
        .inTable("project")
        .onDelete("CASCADE");
    });
};

exports.down = function (knex) {
  return knex.schema.dropTable("tasks").dropTable("project").dropTable("users");
};
