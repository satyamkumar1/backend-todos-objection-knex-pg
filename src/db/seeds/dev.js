/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  
  await knex.raw('truncate table "users" CASCADE');
  await knex.raw('truncate table "project" CASCADE');
  await knex.raw('truncate table "tasks" CASCADE');
  
  await knex('users').insert(
    [{
      name:"satyam",
      email:"satamakumar@gmail.com"
    },{
      name:"shivam",
      email:"shivamkumar@gmail.com"

    }]
  ) 

  await knex('project').insert(
    [{
      title:"satyam",
      description:"will do",
      user_id:1
    },{
      title:"shivam",
      description:"done",
      user_id:2

    }]
  ) 

  await knex('tasks').insert(
    [{
      title:"satyam",
      description:"will do",
      user_id:1,
      project_id:1
    },{
      title:"shivam",
      description:"done",
      user_id:2,
      project_id:2

    }]
  ) 

};
