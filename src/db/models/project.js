const { Model } = require("objection");
const tasks = require("./tasks");

class project extends Model {
  static get tableName() {
    return "project";
  }

  static get relationMappings() {
    const user = require("./users");

    return {
      users: {
        relation: Model.HasOneRelation,
        modelClass: user,
        join: {
          from: "project.user_id",
          to: "users.id",
        },
      },
      tasks: {
        relation: Model.HasManyRelation,
        modelClass: tasks,
        join: {
          from: "project.id",
          to: "tasks.project_id",
        },
      },
    };
  }
}

module.exports = project;
