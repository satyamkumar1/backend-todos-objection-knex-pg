const { Model } = require("objection");

class tasks extends Model {
  static get tableName() {
    return "tasks";
  }
  static get relationMappings() {
    const project = require("./project");

    return {
      project: {
        relation: Model.HasOneRelation,
        modelClass: project,
        join: {
          from: "tasks.project_id",
          to: "project.id",
        },
      },
    };
  }
}

module.exports = tasks;
