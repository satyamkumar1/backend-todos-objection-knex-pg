const bcrypt = require("bcrypt");
const saltRounds = 10;

 function hashPassword(password) {
  const hashpassword = bcrypt.hash(password, saltRounds);
  return hashpassword;
}

function comparePassword(password,hashpassword) {
  return bcrypt.compare(password, hashpassword);
}

module.exports = { hashPassword, comparePassword };
