const jwt = require("jsonwebtoken");
const config = require("../../config");

function generateToken(user) {
  // console.log(user, "<<<===========");
  try {
    return jwt.sign({ id: user[0].id }, config.SECRET_KEY);
  } catch (err) {
    res.json({ success: false, user: "invalid authorization" });
  }
}

function authenticateToken(req, res, next) {
  const token = req.header("Authorization");
  try {
    if (!token) {
      return res.status(401).json({ success: false, data: "Unauthorized" });
    }

    const payload = jwt.verify(token, config.SECRET_KEY);
    req.user = payload;
    // console.log(user, "<<<<===========");
    next();
  } catch (err) {
    res.json({ success: false, user: "invalid authentication" });
  }
}
module.exports = { generateToken, authenticateToken };
