// Update with your config settings.
const config = require("./config");

// console.log(config,"<<<<<<<<<<<<<<<<<<<<")

module.exports = {
  development: {
    client: "postgresql",
    connection: {
      connectionString: config.DBCONFIG,
      ssl: { rejectUnauthorized: false },
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: "./src/db/migrations"
    },
    seeds: {
      directory: "./seeds",
    },
  },
};
