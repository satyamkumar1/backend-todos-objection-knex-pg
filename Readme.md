# Basic Project TODO API Documentation
- The Basic Project TODO API allows users to manage projects and tasks within those projects. Users can create, update, and delete projects, as well as mark tasks as done. The API supports user authentication for secure access.

## Table of Contents
- Introduction
- Authentication
- Endpoints
     - User Routes
     - Project Routes
     - Tasks Routes
- Error Handling
- Rate Limiting
- Examples
- Support and Contact

### Introduction
The Basic Project TODO API provides a simple interface to manage projects and tasks. Users can create projects with titles, descriptions, and tasks. Tasks can be marked as done, and when all tasks within a project are done, the project itself can be marked as done.

### Authentication
Authentication is required to access the API. Users must include their authentication token in the request headers to make authorized requests. Refer to the Authentication section for more details.

### Endpoints
- User Routes
###  Create a User

Route: https://todoas-api-satyam.onrender.com/api/user

- request


        {
        "name": "john_doe",
        "email": "john@example.com",
        "password": "secretpassword"
        }
- response

        {
        "success": true,
        "message": "User created successfully",
        
        }

### Get User Profile
    - Route: https://todoas-api-satyam.onrender.com/api/user/:name
- Response:

        {
        "success": true,
        "user": {
            "username": "john_doe",
            "email": "john@example.com"           
         }
        }


## Project Routes
### Create a Project
- Route: https://todoas-api-satyam.onrender.com/api/project
- Request:

        {
        "title": "My Project",
        "description": "This is a sample project",              
        }        

- Sample Response:

        {
        "success": true,
        "message": "Project created successfully",
        
        }
- Description: Creates a new project with the provided title, description, and tasks.

### Get All Porject Details
- Route: https://todoas-api-satyam.onrender.com/api/project/
- Sample Response:

        {
        "success": true,
        "data":[ "proejct": {
            "id": 1,
            "title": "My tasks",
            "description": "This is a sample tasks",
            
        }]
        }
- Description: Retrieves details of the all project

### Get Project Details
- Route: https://todoas-api-satyam.onrender.com/api/project/:id
- Sample Response:

        {
        "success": true,
        "project": {
            "id": 1,
            "title": "My Project",
            "description": "This is a sample project",
            
        }
        }
- Description: Retrieves details of the project with the specified ID.
### Update Project Details
- Route: https://todoas-api-satyam.onrender.com/api/project/:id
- Sample Request:

        {
        "title": "Updated Project Title",
        "description": "Updated project description",
        }
- Sample Response:

        {
        "success": true,
        "message": "Project updated successfully",

        }
- Description: Updates the details of the project with the specified ID.
### Delete a Project
- Route:  https://todoas-api-satyam.onrender.com/api/project/:id
- Sample Response:

        {
        "success": true,
        "message": "Project deleted successfully"
        }
- Description: Deletes the project with the specified ID.


## Tasks Routes

### Create a tasks
- Route: https://todoas-api-satyam.onrender.com/api/tasks/
- Request:

        {
        "title": "My Project",
        "description": "This is a sample project",
        "project_id:1,    
        }        

- Sample Response:

        {
        "success": true,
        "message": "tasks created successfully",
        
        }
- Description: Creates a new tasks with the provided title, description, and tasks.
### Get All Tasks Details
- Route: https://todoas-api-satyam.onrender.com/api/tasks/
- Sample Response:

        {
        "success": true,
        "data":[ "tasks": {
            "id": 1,
            "title": "My tasks",
            "description": "This is a sample tasks",
            
        }]
        }
- Description: Retrieves details of the all tasks

### Get Tasks Details
- Route: https://todoas-api-satyam.onrender.com/api/tasks/:id
- Sample Response:

        {
        "success": true,
        "tasks": {
            "id": 1,
            "title": "My tasks",
            "description": "This is a sample tasks",
            
        }
        }
- Description: Retrieves details of the tasks with the specified ID.
### Update Tasks Details
- Route: https://todoas-api-satyam.onrender.com/api/tasks/:id
- Sample Request:

        {
        "title": "Updated tasks Title",
        "description": "Updated tasks description",
        }
- Sample Response:

        {
        "success": true,
        "message": "tasks updated successfully",

        }
- Description: Updates the details of the tasks with the specified ID.
### Delete a tasks
- Route:  https://todoas-api-satyam.onrender.com/api/tasks/:id
- Sample Response:

        {
        "success": true,
        "message": "Project deleted successfully"
        }
- Description: Deletes the tasks with the specified ID.



### Error Handling
- The API returns standardized error responses with appropriate HTTP status codes. Refer to the Error Handling section for more details.

### Rate Limiting
- The API has rate limiting in place to prevent abuse. Refer to the Rate Limiting section for more details.

### Examples
- For detailed examples and use cases, refer to the Examples section.

### Support and Contact
For support or questions, please contact our support team at support@example.com.



