const express = require("express");
const app = express();
const db = require("./src/db/db-setup");
const userdata = require("./src/controller/users");
const projectdata = require("./src/controller/project");
const taskdata=require('./src/controller/tasks');
const cors=require('cors');
app.use(cors());
db();
app.use(express.json());

app.use("/api/users", userdata);
app.use("/api/project", projectdata);
app.use("/api/tasks",taskdata);

app.use((req,res,next) => {
  return res.status(404).json({msg:"no url found"});
})










app.listen(8080, () => {
  console.log("server started");
});
